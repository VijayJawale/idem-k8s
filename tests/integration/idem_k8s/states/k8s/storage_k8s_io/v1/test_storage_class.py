import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_storage_class(hub, ctx):
    k8s_storage_class_temp_name = "idem-test-k8s-storage-class-" + str(uuid.uuid4())
    metadata = {"name": k8s_storage_class_temp_name}
    parameters = {"type": "gp2", "iopsPerGB": "10"}
    provisioner = "kubernetes.io/aws-ebs"
    allowed_topologies = [
        {
            "match_label_expressions": [
                {"key": "env", "values": ["dev"]},
                {"key": "app", "values": ["idem-test"]},
            ]
        }
    ]
    mount_options = ["soft", "ro"]
    reclaim_policy = "Delete"
    volume_binding_mode = "WaitForFirstConsumer"

    # Create k8s_storage_class with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.k8s.storage_k8s_io.v1.storage_class.present(
        test_ctx,
        name=k8s_storage_class_temp_name,
        metadata=metadata,
        provisioner=provisioner,
        allowed_topologies=allowed_topologies,
        mount_options=mount_options,
        parameters=parameters,
        reclaim_policy=reclaim_policy,
        volume_binding_mode=volume_binding_mode,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would create k8s.storage_k8s_io.v1.storage_class '{k8s_storage_class_temp_name}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    assert_storage_class(
        metadata,
        provisioner,
        allowed_topologies,
        mount_options,
        parameters,
        reclaim_policy,
        volume_binding_mode,
        ret.get("new_state"),
        "resource_id_known_after_present",
    )

    # Create k8s_storage_class in real
    ret = await hub.states.k8s.storage_k8s_io.v1.storage_class.present(
        ctx,
        name=k8s_storage_class_temp_name,
        metadata=metadata,
        provisioner=provisioner,
        allowed_topologies=allowed_topologies,
        mount_options=mount_options,
        parameters=parameters,
        reclaim_policy=reclaim_policy,
        volume_binding_mode=volume_binding_mode,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Created k8s.storage_k8s_io.v1.storage_class '{k8s_storage_class_temp_name}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    assert_storage_class(
        metadata,
        provisioner,
        allowed_topologies,
        mount_options,
        parameters,
        reclaim_policy,
        volume_binding_mode,
        ret.get("new_state"),
    )
    resource_id = ret.get("new_state").get("resource_id")

    # Describe k8s_storage_class
    describe_ret = await hub.states.k8s.storage_k8s_io.v1.storage_class.describe(ctx)
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "k8s.storage_k8s_io.v1.storage_class.present" in describe_ret.get(
        resource_id
    )
    described_resource = describe_ret.get(resource_id).get(
        "k8s.storage_k8s_io.v1.storage_class.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert_storage_class(
        metadata,
        provisioner,
        allowed_topologies,
        mount_options,
        parameters,
        reclaim_policy,
        volume_binding_mode,
        described_resource_map,
    )

    # Update k8s_storage_class with test flag
    updated_allowed_topologies = [
        {
            "match_label_expressions": [
                {"key": "env", "values": ["test"]},
                {"key": "app", "values": ["idem-test"]},
            ]
        }
    ]
    updated_mount_options = ["soft"]

    ret = await hub.states.k8s.storage_k8s_io.v1.storage_class.present(
        test_ctx,
        name=k8s_storage_class_temp_name,
        metadata=metadata,
        provisioner=provisioner,
        allowed_topologies=updated_allowed_topologies,
        mount_options=updated_mount_options,
        parameters=parameters,
        reclaim_policy=reclaim_policy,
        volume_binding_mode=volume_binding_mode,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would update k8s.storage_k8s_io.v1.storage_class '{k8s_storage_class_temp_name}'"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    assert_storage_class(
        metadata,
        provisioner,
        allowed_topologies,
        mount_options,
        parameters,
        reclaim_policy,
        volume_binding_mode,
        ret.get("old_state"),
    )
    assert_storage_class(
        metadata,
        provisioner,
        updated_allowed_topologies,
        updated_mount_options,
        parameters,
        reclaim_policy,
        volume_binding_mode,
        ret.get("new_state"),
    )

    # Update k8s_storage_class in real
    ret = await hub.states.k8s.storage_k8s_io.v1.storage_class.present(
        ctx,
        name=k8s_storage_class_temp_name,
        metadata=metadata,
        provisioner=provisioner,
        allowed_topologies=updated_allowed_topologies,
        mount_options=updated_mount_options,
        parameters=parameters,
        reclaim_policy=reclaim_policy,
        volume_binding_mode=volume_binding_mode,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Updated k8s.storage_k8s_io.v1.storage_class '{k8s_storage_class_temp_name}'"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    assert_storage_class(
        metadata,
        provisioner,
        allowed_topologies,
        mount_options,
        parameters,
        reclaim_policy,
        volume_binding_mode,
        ret.get("old_state"),
    )
    assert_storage_class(
        metadata,
        provisioner,
        updated_allowed_topologies,
        updated_mount_options,
        parameters,
        reclaim_policy,
        volume_binding_mode,
        ret.get("new_state"),
    )

    # Delete k8s_storage_class with test flag
    ret = await hub.states.k8s.storage_k8s_io.v1.storage_class.absent(
        test_ctx, name=k8s_storage_class_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Would delete k8s.storage_k8s_io.v1.storage_class '{k8s_storage_class_temp_name}'"
        in ret["comment"]
    )
    assert_storage_class(
        metadata,
        provisioner,
        updated_allowed_topologies,
        updated_mount_options,
        parameters,
        reclaim_policy,
        volume_binding_mode,
        ret.get("old_state"),
    )

    # Delete k8s_storage_class in real
    ret = await hub.states.k8s.storage_k8s_io.v1.storage_class.absent(
        ctx, name=k8s_storage_class_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Deleted k8s.storage_k8s_io.v1.storage_class '{k8s_storage_class_temp_name}'"
        in ret["comment"]
    )
    assert_storage_class(
        metadata,
        provisioner,
        updated_allowed_topologies,
        updated_mount_options,
        parameters,
        reclaim_policy,
        volume_binding_mode,
        ret.get("old_state"),
    )

    # Deleting k8s_storage_class again should be an no-op
    ret = await hub.states.k8s.storage_k8s_io.v1.storage_class.absent(
        ctx, name=k8s_storage_class_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert (
        f"k8s.storage_k8s_io.v1.storage_class '{k8s_storage_class_temp_name}' already absent"
        in ret["comment"]
    )


def assert_storage_class(
    metadata,
    provisioner,
    allowed_topologies,
    mount_options,
    parameters,
    reclaim_policy,
    volume_binding_mode,
    resource,
    resource_id=None,
):
    assert metadata.get("name") == resource.get("metadata").get("name")
    assert provisioner == resource.get("provisioner")
    assert allowed_topologies == resource.get("allowed_topologies")
    assert mount_options == resource.get("mount_options")
    assert parameters == resource.get("parameters")
    assert reclaim_policy == resource.get("reclaim_policy")
    assert volume_binding_mode == resource.get("volume_binding_mode")

    if resource_id:
        assert resource_id == resource.get("resource_id")
    else:
        assert metadata.get("name") == resource.get("resource_id")
