import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_cluster_role_binding(hub, ctx):
    k8s_cluster_role_binding_temp_name = "idem-test-k8s-cluster_role_binding-" + str(
        uuid.uuid4()
    )
    metadata_name = k8s_cluster_role_binding_temp_name
    metadata = {"name": metadata_name, "labels": {"name": "development-1"}}
    role_ref = {
        "api_group": "rbac.authorization.k8s.io",
        "kind": "ClusterRole",
        "name": "idem-test-cluster-role-binding",
    }

    subjects = [
        {
            "api_group": "rbac.authorization.k8s.io",
            "kind": "User",
            "name": "idem-test-cluster-role-binding",
        }
    ]

    # create k8s_cluster_role_binding with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.k8s.rbac.v1.cluster_role_binding.present(
        test_ctx,
        name=k8s_cluster_role_binding_temp_name,
        metadata=metadata,
        role_ref=role_ref,
        subjects=subjects,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would create k8s.rbac.v1.cluster_role_binding '{k8s_cluster_role_binding_temp_name}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    assert_cluster_role_binding(
        subjects,
        role_ref,
        metadata,
        metadata_name,
        ret.get("new_state"),
        "development-1",
        "resource_id_known_after_present",
    )

    # create real k8s_cluster_role_binding
    ret = await hub.states.k8s.rbac.v1.cluster_role_binding.present(
        ctx,
        name=k8s_cluster_role_binding_temp_name,
        metadata=metadata,
        role_ref=role_ref,
        subjects=subjects,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Created k8s.rbac.v1.cluster_role_binding '{k8s_cluster_role_binding_temp_name}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    assert_cluster_role_binding(
        subjects, role_ref, metadata, metadata_name, ret.get("new_state")
    )

    resource_id = ret.get("new_state").get("resource_id")

    # Describe k8s_cluster_role_binding
    describe_ret = await hub.states.k8s.rbac.v1.cluster_role_binding.describe(ctx)
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "k8s.rbac.v1.cluster_role_binding.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get(
        "k8s.rbac.v1.cluster_role_binding.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert_cluster_role_binding(
        subjects, role_ref, metadata, metadata_name, described_resource_map
    )

    # update k8s_cluster_role_binding with test flag
    updated_metadata = copy.deepcopy(metadata)
    updated_metadata["labels"] = {"name": "development-test"}
    ret = await hub.states.k8s.rbac.v1.cluster_role_binding.present(
        test_ctx,
        name=k8s_cluster_role_binding_temp_name,
        metadata=updated_metadata,
        resource_id=resource_id,
        role_ref=role_ref,
        subjects=subjects,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        f"Would update k8s.rbac.v1.cluster_role_binding '{k8s_cluster_role_binding_temp_name}'"
        in ret["comment"]
    )
    assert_cluster_role_binding(
        subjects, role_ref, metadata, metadata_name, ret.get("old_state")
    )
    assert_cluster_role_binding(
        subjects,
        role_ref,
        metadata,
        metadata_name,
        ret.get("new_state"),
        "development-test",
    )

    # real update k8s_cluster_role_binding
    ret = await hub.states.k8s.rbac.v1.cluster_role_binding.present(
        ctx,
        name=k8s_cluster_role_binding_temp_name,
        metadata=updated_metadata,
        resource_id=resource_id,
        role_ref=role_ref,
        subjects=subjects,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        f"Updated k8s.rbac.v1.cluster_role_binding '{k8s_cluster_role_binding_temp_name}'"
        in ret["comment"]
    )
    assert_cluster_role_binding(
        subjects, role_ref, metadata, metadata_name, ret.get("old_state")
    )
    assert_cluster_role_binding(
        subjects,
        role_ref,
        metadata,
        metadata_name,
        ret.get("new_state"),
        "development-test",
    )

    # Delete k8s_cluster_role_binding with test flag
    ret = await hub.states.k8s.rbac.v1.cluster_role_binding.absent(
        test_ctx, name=k8s_cluster_role_binding_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Would delete k8s.rbac.v1.cluster_role_binding '{k8s_cluster_role_binding_temp_name}'"
        in ret["comment"]
    )
    assert_cluster_role_binding(
        subjects,
        role_ref,
        metadata,
        metadata_name,
        ret.get("old_state"),
        "development-test",
    )

    # Delete k8s_cluster_role_binding with real account
    ret = await hub.states.k8s.rbac.v1.cluster_role_binding.absent(
        ctx, name=k8s_cluster_role_binding_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Deleted k8s.rbac.v1.cluster_role_binding '{k8s_cluster_role_binding_temp_name}'"
        in ret["comment"]
    )
    assert_cluster_role_binding(
        subjects,
        role_ref,
        metadata,
        metadata_name,
        ret.get("old_state"),
        "development-test",
    )

    # Deleting k8s_cluster_role_binding again should be an no-op
    ret = await hub.states.k8s.rbac.v1.cluster_role_binding.absent(
        ctx, name=k8s_cluster_role_binding_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert (
        f"k8s.rbac.v1.cluster_role_binding '{k8s_cluster_role_binding_temp_name}' already absent"
        in ret["comment"]
    )


def assert_cluster_role_binding(
    subjects,
    role_ref,
    metadata,
    metadata_name,
    resource,
    label_value="development-1",
    resource_id=None,
):
    assert subjects == resource.get("subjects")
    assert role_ref == resource.get("role_ref")
    assert metadata.get("cluster_role_binding") == resource.get("metadata").get(
        "cluster_role_binding"
    )
    assert metadata.get("name") == resource.get("metadata").get("name")
    if not resource_id:
        assert metadata_name == resource.get("resource_id")
    else:
        assert resource_id == resource.get("resource_id")
    assert label_value == resource.get("metadata").get("labels").get("name")
